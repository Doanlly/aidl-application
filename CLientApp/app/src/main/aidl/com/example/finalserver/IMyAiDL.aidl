// IMyAIDL.aidl
package com.example.finalserver;

// Declare any non-default types here with import statements
import com.example.finalserver.User;
import com.example.finalserver.ChatMessage;

parcelable User;
parcelable ChatMessage;
interface IMyAiDL {

    /** Set displayed value of screen */
    void setDisplayedValue(String packageName, String data, in ChatMessage message);
    long getMessageTime();
    ChatMessage getChatMessage();
    List<ChatMessage> getStory();
    List<User> getUser();
}
 //List<ChatMessage> getChatMessagesBySenderReceiver(int senderId,int receiverId);