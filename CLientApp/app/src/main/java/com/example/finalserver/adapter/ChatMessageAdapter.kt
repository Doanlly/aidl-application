package com.example.finalserver.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.recyclerview.widget.RecyclerView
import com.example.clientapp.databinding.ItemChatEssageBinding
import com.example.finalserver.ChatMessage
import com.example.finalserver.User
import com.example.finalserver.viewmodel.ChatMessageViewModel

class ChatMessageAdapter(
    private val chatMessageViewModel: ChatMessageViewModel,
    private val onChatMessageClickListener: (ChatMessage) -> Unit
) :
    RecyclerView.Adapter<ChatMessageAdapter.ViewHolder>() {
    init {
        chatMessageViewModel.userItem().observeForever { chatMessages ->
            this.chatMessages = chatMessages
            notifyDataSetChanged()
        }
    }

    private var chatMessages: List<ChatMessage> = emptyList()
    private var listUser: List<User> = emptyList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemChatEssageBinding.inflate(inflater, parent, false)

        return ViewHolder(binding)//,parent.context)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val chatMessage = chatMessages[position]
        holder.bind(chatMessage)
    }

    private fun findUserNameById(chatMessage: ChatMessage, userList: List<User>): String? {
        val receiverId = chatMessage.receiverId
        val user = userList.find { it.id == receiverId }
        return user?.name
    }

    override fun getItemCount(): Int {
        return chatMessages.size
    }

    inner class ViewHolder(private val binding: ItemChatEssageBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(chatMessage: ChatMessage) {
            val name = findUserNameById(chatMessage, listUser)

            binding.recentMesTxt.text = chatMessage.messageContent
            binding.btnTimeSent.text = chatMessage.messageTime.toString()
            binding.nameTxtItem.text = name
            binding.cartViewItem.setOnClickListener {
                Log.d("CLick chatMessage", "id: ${chatMessage.id}- ${chatMessage.messageContent}")
                onChatMessageClickListener.invoke(chatMessage)
            }
        }
    }

    fun submitList(chatMessages: List<ChatMessage>, listUser: List<User>) {
        this.chatMessages = chatMessages
        this.listUser = listUser
        notifyDataSetChanged()

    }
}