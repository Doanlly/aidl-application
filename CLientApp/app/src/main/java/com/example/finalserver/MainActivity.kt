package com.example.finalserver

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.clientapp.R
import com.pmirkelam.ipcserver.ui.aidl.AidlFragment


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Navigate to the AidlFragment
        supportFragmentManager.beginTransaction()
            .replace(R.id.nav_host_fragment, AidlFragment())
            .commit()
    }
}