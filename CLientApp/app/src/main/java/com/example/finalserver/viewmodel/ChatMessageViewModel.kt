package com.example.finalserver.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.finalserver.ChatMessage
import com.example.finalserver.User

class ChatMessageViewModel : ViewModel() {

    private val chatMessagesLiveData = MutableLiveData<List<ChatMessage>>()
    private val userLiveData = MutableLiveData<List<User>>()
    private val userItem = MutableLiveData<List<ChatMessage>>()

    // Trong ChatMessageViewModel.kt
    fun getChatMessagesLiveData(): LiveData<List<ChatMessage>> {
        return chatMessagesLiveData
    }

    fun updateChatMessages(chatMessages: List<ChatMessage>) {
        chatMessagesLiveData.value = chatMessages
        userItem.value = filterMessagesByConversation(chatMessagesLiveData.value!!)
    }

    fun getChatsUserLiveData(): LiveData<List<User>> {
        return userLiveData
    }

    fun updateUsers(user: List<User>) {
        userLiveData.value = user
    }

    fun userItem(): LiveData<List<ChatMessage>> {
        return userItem
    }

    fun getFilteredChatMessages(
        senderId: Int,
        receiverId: Int
    ): MutableLiveData<List<ChatMessage>?> {
        val filteredChatMessages = MutableLiveData<List<ChatMessage>?>()

        // Lấy danh sách chatMessages hiện tại
        val currentChatMessages = chatMessagesLiveData.value

        val filteredList = currentChatMessages?.filter { message ->
            message.senderId == senderId && message.receiverId == receiverId
        }

        if (filteredList != null) {
            if (filteredList.isNotEmpty()) {
                filteredChatMessages.value = filteredList
            }
        }
        return filteredChatMessages
    }

    private fun filterMessagesByConversation(chatMessages: List<ChatMessage>): List<ChatMessage> {
        val messagesByConversationId = HashMap<Pair<Int, Int>, ChatMessage>()
        val newList = mutableListOf<ChatMessage>()

        for (chatMessage in chatMessages) {
            val conversationId = chatMessage.senderId
            val conversationReceiverId = chatMessage.receiverId
            val pair = Pair(conversationId, conversationReceiverId)
            messagesByConversationId[pair] = chatMessage

        }

        // Lấy tất cả các bản ghi duy nhất từ danh sách
        val conversationIds = messagesByConversationId.keys.toList()
        for (conversationId in conversationIds) {
            val messages = messagesByConversationId[conversationId]!!
            newList.add(messages)
        }

        return newList
    }

}
