package com.pmirkelam.ipcserver

data class Client(
    var clientPackageName: String?,
    var clientData: String?,
    var name:String?,
    var id:Int?,
    var imgPath:String?,
//    val iRemoteService: IMyAiDL?,
    //var student:Student?
    //var student: Student
)

object RecentClient {
    var client: Client? = null
}