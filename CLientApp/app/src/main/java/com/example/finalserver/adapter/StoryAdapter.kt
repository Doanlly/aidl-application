package com.example.finalserver.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.clientapp.R
import com.example.clientapp.databinding.ItemStoryReceiverBinding
import com.example.clientapp.databinding.ItemStorySenderBinding
import com.example.finalserver.ChatMessage

class StoryAdapter(

) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        const val VIEW_TYPE_SENT = 1
        const val VIEW_TYPE_RECEIVED = 2
    }

    private var data: MutableList<ChatMessage> = mutableListOf()
    private lateinit var itemStorySender: ItemStorySenderBinding
    private lateinit var itemStoryReceiver: ItemStoryReceiverBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return if (viewType == VIEW_TYPE_SENT) {
            itemStorySender = ItemStorySenderBinding.inflate(inflater, parent, false)
            SendMessageViewHolder(itemStorySender)
        } else {
            itemStoryReceiver = ItemStoryReceiverBinding.inflate(inflater, parent, false)
            ReceiverMessageViewHolder(itemStoryReceiver)
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val message = data[position]
        if (holder is SendMessageViewHolder) {
            holder.setMessageSendData(message)
        } else if (holder is ReceiverMessageViewHolder) {
            holder.setMessageReceiverData(message)
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (data[position].senderId == data[position].checkMessageToID) {
            return VIEW_TYPE_RECEIVED
        }
        return VIEW_TYPE_SENT
    }

    inner class SendMessageViewHolder(private val binding: ItemStorySenderBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun setMessageSendData(chatMessage: ChatMessage) {
            binding.nameTxtItemSender.text = chatMessage.messageContent
            binding.btnTimeSentSender.text = chatMessage.messageTime
        }
    }

    inner class ReceiverMessageViewHolder(private val binding: ItemStoryReceiverBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun setMessageReceiverData(chatMessage: ChatMessage) {
            binding.nameTxtItemReceiver.text = chatMessage.messageContent
            binding.btnTimeSentReceiver.text = chatMessage.messageTime
        }
    }

    fun updateList(chatMessages: List<ChatMessage>) {
        data.clear()
        data.addAll(chatMessages)
        notifyDataSetChanged()
    }

}