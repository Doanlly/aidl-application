package com.pmirkelam.ipcserver.ui.aidl

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import android.util.Log
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.clientapp.R
import com.example.clientapp.databinding.FragmentAidlBinding
import com.example.finalserver.IMyAiDL
import com.example.finalserver.User
import com.example.finalserver.ChatMessage
import com.example.finalserver.adapter.ChatMessageAdapter
import com.example.finalserver.viewmodel.ChatMessageViewModel


class AidlFragment : Fragment(), ServiceConnection {
    private var _binding: FragmentAidlBinding? = null
    private val binding get() = _binding!!
    private var recyclerView: RecyclerView? = null
    private var iRemoteService: IMyAiDL? = null
    private var connected = false
    private var chatMessagesList: MutableList<ChatMessage>? = null
    private lateinit var chatMessageViewModel: ChatMessageViewModel
    private lateinit var chatMessageAdapter: ChatMessageAdapter
    private lateinit var listUser: List<User>
    private val handler = android.os.Handler(Looper.getMainLooper())

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAidlBinding.inflate(inflater, container, false)
        val view = binding.root
        connectToRemoteService()
        chatMessageViewModel = ViewModelProvider(this)[ChatMessageViewModel::class.java]
        chatMessageAdapter = ChatMessageAdapter(chatMessageViewModel) { chatMessage ->
            val bundle = Bundle()
            bundle.apply {
                putParcelable("chatMessage", chatMessage)
                putBinder("iRemoteService", iRemoteService?.asBinder())
            }
            //Log.d("ChatMessage StoryMessage:", "==> $chatMessage")
            findNavController().navigate(R.id.storyMessage, bundle)
        }

        recyclerView = view.findViewById(R.id.recyclerview)

        recyclerView?.layoutManager = LinearLayoutManager(requireContext())
        recyclerView?.adapter = chatMessageAdapter

        chatMessageViewModel.userItem().observe(viewLifecycleOwner) { chatMessages ->
            chatMessageAdapter.submitList(chatMessages, listUser) ?: 0

        }
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Log.d("GUi lai,", "chua duoc")
        connectToRemoteService()
    }


    private fun connectToRemoteService() {
        val intent = Intent("aidlexample")
        val pack = IMyAiDL::class.java.`package`
        pack?.let {
            intent.setPackage(pack.name)
            activity?.applicationContext?.bindService(
                intent, this, Context.BIND_AUTO_CREATE
            )
        }
    }

    override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
        val UserInfo = User(20, "John Doe", "imgA")
        iRemoteService = IMyAiDL.Stub.asInterface(service)
        chatMessagesList = iRemoteService?.story!!
        listUser = iRemoteService?.user!!
        chatMessageViewModel.updateUsers(listUser)
        chatMessageViewModel.updateChatMessages(iRemoteService?.story!!)

        chatMessageViewModel.userItem().observe(viewLifecycleOwner) { chatMessages ->
            chatMessageAdapter.submitList(chatMessages, listUser)
        }

        chatMessageViewModel.getFilteredChatMessages(1, 2).observe(
            viewLifecycleOwner
        ) { chatMessages ->
            Log.d("Check userItem", "${chatMessages}")
        }

        Log.d("Check story", "${iRemoteService?.story!!}")

//        if (connected && chatMessagesList != iRemoteService?.story!!) {
//            chatMessageViewModel.updateChatMessages(iRemoteService?.story!!)
//            chatMessageViewModel.updateUsers(listUser)
//            chatMessageViewModel.userItem().observe(viewLifecycleOwner) { chatMessages ->
//                chatMessageAdapter.submitList(chatMessages, listUser)
//            }
//        }
        //handler.post(updateRunnable) // Bắt đầu cập nhật tự động
        connected = true
    }

    private val updateRunnable = object : Runnable {
        override fun run() {
            if (connected) {
                chatMessageViewModel.updateChatMessages(iRemoteService?.story!!)
                chatMessageViewModel.updateUsers(listUser)
                chatMessageViewModel.userItem().observe(viewLifecycleOwner) { chatMessages ->
                    chatMessageAdapter.submitList(chatMessages, listUser)
                }
            }
            // Thực hiện sau mỗi khoảng thời gian, ở đây là 1 giây (1000 milliseconds)
            handler.postDelayed(this, 1000)
        }
    }

    override fun onServiceDisconnected(name: ComponentName?) {
        iRemoteService = null
        connected = false
    }

    override fun onResume() {
        super.onResume()
        if (connected && chatMessagesList != iRemoteService?.story!!) {
            chatMessageViewModel.updateChatMessages(iRemoteService?.story!!)
            chatMessageViewModel.updateUsers(iRemoteService?.user!!)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}

