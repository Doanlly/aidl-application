package com.example.finalserver.ui.aidl

import android.os.Bundle
import android.os.Looper
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageButton
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.clientapp.R
import com.example.clientapp.databinding.FragmentStoryMessageBinding
import com.example.finalserver.ChatMessage
import com.example.finalserver.IMyAiDL
import com.example.finalserver.User
import com.example.finalserver.adapter.StoryAdapter
import com.example.finalserver.viewmodel.ChatMessageViewModel
import java.text.SimpleDateFormat


class StoryMessage : Fragment() {
    // Khai báo một biến để kiểm tra xem View đã được tạo hay chưa
    private var isViewCreated = false
    private var isDataChanged = false
    private val binding get() = _binding!!
    private var chatMessage: ChatMessage? = null
    private var iRemoteService: IMyAiDL? = null
    private var recyclerView: RecyclerView? = null
    private var _binding: FragmentStoryMessageBinding? = null
    private var chatMessageViewModel: ChatMessageViewModel? = null
    private var chatMessagesList: MutableList<ChatMessage>? = null
    private lateinit var adapter: StoryAdapter
    private val handler = android.os.Handler(Looper.getMainLooper())
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            chatMessage = it.getParcelable<ChatMessage>("chatMessage")
            Log.d("ChatMessage StoryMessage:", "==> $chatMessage")
            val binder = it.getBinder("iRemoteService")
            iRemoteService = IMyAiDL.Stub.asInterface(binder)
            Log.d("iRemoteService?.story!!", "${iRemoteService?.story!!}")
            chatMessagesList = iRemoteService?.story!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        isViewCreated = true
        _binding = FragmentStoryMessageBinding.inflate(inflater, container, false)
        val view = binding.root
        chatMessageViewModel =
            ViewModelProvider(requireActivity()).get(ChatMessageViewModel::class.java)
        chatMessageViewModel?.updateChatMessages(chatMessagesList!!)
        binding.txtNameUser.text = iRemoteService?.user?.let {
            chatMessage?.let { it1 ->
                findUserNameById(
                    it1, it
                )
            }
        }

        adapter = StoryAdapter()
        recyclerView = binding.recyclerViewChatStory
        recyclerView?.adapter = adapter
        recyclerView?.layoutManager = LinearLayoutManager(context)

        chatMessage?.let {
            chatMessageViewModel?.getFilteredChatMessages(it.senderId, chatMessage!!.receiverId)
                ?.observe(
                    viewLifecycleOwner
                ) { chatMessage ->
                    Log.d("Check List:", "$chatMessage")
                    adapter.updateList(chatMessage!!)
                    recyclerView?.scrollToPosition(adapter.itemCount - 1)
                }
        }
        handler.postDelayed(object : Runnable {
            override fun run() {
                handleClientData()
                handler.postDelayed(this, 500) // Gọi lại sau 2 giây
            }
        }, 500)

        binding.btnSentChat.setOnClickListener {
            val simpleDateFormat = SimpleDateFormat("hh:mm a")
            val messageTime = System.currentTimeMillis()
            val formattedMessageTime = simpleDateFormat.format(messageTime)
            val messageContent =
                binding.edtTextSent.text.toString() ?: "" // Lấy nội dung từ editText

            val chat = ChatMessage(
                0,
                chatMessage!!.senderId,
                chatMessage!!.receiverId,
                chatMessage!!.receiverId,
                formattedMessageTime,
                messageContent
            )
            val time = getCurrentTimeInMillis().toString()
            if (messageContent.isNotEmpty()) {
                chatMessagesList?.add(chat)
                chatMessageViewModel!!.updateChatMessages(chatMessagesList!!)
                chatMessageViewModel!!.getFilteredChatMessages(
                    chatMessage!!.senderId,
                    chatMessage!!.receiverId
                ).observe(viewLifecycleOwner) { chatMessages ->
                    adapter.updateList(chatMessages!!)
                    recyclerView?.scrollToPosition(adapter.itemCount - 1)
                }
                iRemoteService?.setDisplayedValue(
                    requireContext().packageName,
                    time,
                    chat
                )
            }
            binding.edtTextSent.text.clear()

            Log.d("click button", "${chatMessagesList}")


        }

        binding.btnBackButton.setOnClickListener {
            findNavController().navigate(R.id.action_storyMessage_to_aidlFragment)
        }
        return view
    }

    private fun getCurrentTimeInMillis(): Long {
        return System.currentTimeMillis()
    }

    private var timenew: Long = 0L
    fun handleClientData() {
        if (isViewCreated && !isDestroyed) {
            //val currentViewLifecycleOwner = viewLifecycleOwner
            handler.post {

                var messageTime = iRemoteService!!.messageTime
                if (timenew != messageTime && messageTime != 0L && timenew != 0L) {
                    Log.d("time", "$timenew")
                    Log.d("time messageTime", "$messageTime")

                    Log.d("CHeck tin nhan", "${iRemoteService!!.messageTime}")
                    chatMessagesList?.add(iRemoteService!!.chatMessage)
                    chatMessageViewModel?.updateChatMessages(chatMessagesList!!)
                    chatMessageViewModel?.getFilteredChatMessages(
                        chatMessage!!.senderId,
                        chatMessage!!.receiverId
                    )!!.observe(viewLifecycleOwner) { chatMessageNew ->
                        if (chatMessageNew != null) {
                            adapter.updateList(chatMessageNew)
                            Log.d("CHeck tin nhan", "${chatMessageNew}")

                        }
                        recyclerView?.scrollToPosition(adapter.itemCount - 1)
                    }

                }
                timenew = messageTime

                messageTime = 0
            }
        }
        // time = 0
    }


    private val isDestroyed: Boolean
        get() = activity == null || activity?.isFinishing == true || isRemoving || isDetached


    override fun onResume() {
        super.onResume()
        if (chatMessagesList != null && iRemoteService?.story != null &&
            chatMessagesList != iRemoteService?.story
        ) {
            chatMessageViewModel?.updateChatMessages(iRemoteService?.story!!)
        }
    }

    private fun findUserNameById(chatMessage: ChatMessage, userList: List<User>): String? {
        val receiverId = chatMessage.receiverId
        val user = userList.find { it.id == receiverId }
        return user?.name
    }
}

//    private val updateRunnable = Runnable {
//        if (isViewCreated && !isDestroyed) {
//            val currentViewLifecycleOwner = viewLifecycleOwner
//            val messageTime = iRemoteService!!.messageTime
//            if (messageTime != null && time != messageTime) {
//                iRemoteService?.let {
//                    chatMessagesList?.add(it.chatMessage)
//                    chatMessageViewModel?.updateChatMessages(chatMessagesList!!)
//                }
//                Log.d("CHeck tin nhan", "${iRemoteService!!.messageTime}")
//                chatMessage?.let {
//                    chatMessageViewModel?.getFilteredChatMessages(
//                        it.senderId,
//                        it.receiverId
//                    )?.observe(currentViewLifecycleOwner) { chatMessages ->
//                        if (isViewCreated) {
//                            adapter.updateList(chatMessages!!)
//                            recyclerView?.scrollToPosition(adapter.itemCount - 1)
//                        }
//                    }
//                }
//                Log.d("IremoveService.chat", "${iRemoteService?.chatMessage}")
//                time = messageTime
//            }
//        }
//    }