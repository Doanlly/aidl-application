package com.example.finalserver
import android.os.Parcel
import android.os.Parcelable

data class ChatMessage(
    val id: Int,
    val senderId: Int,
    val receiverId: Int,
    val checkMessageToID:Int,
    val messageTime: String?,
    val messageContent: String?
):Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(senderId)
        parcel.writeInt(receiverId)
        parcel.writeInt(checkMessageToID)
        parcel.writeString(messageTime)
        parcel.writeString(messageContent)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ChatMessage> {
        override fun createFromParcel(parcel: Parcel): ChatMessage {
            return ChatMessage(parcel)
        }

        override fun newArray(size: Int): Array<ChatMessage?> {
            return arrayOfNulls(size)
        }
    }
}