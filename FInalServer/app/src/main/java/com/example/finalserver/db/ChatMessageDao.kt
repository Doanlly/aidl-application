package com.example.finalserver.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.example.finalserver.ChatMessage
import kotlinx.coroutines.flow.Flow

@Dao
interface ChatMessageDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertChatMessage(chatMessage: ChatMessage)
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    suspend fun insertChatMessages(chatMessages: List<ChatMessage>)

    @Update
    suspend fun updateChatMessage(chatMessage: ChatMessage)

    @Query("SELECT MAX(id) FROM chat_message_table")
    suspend fun getMaxId(): Int

    @Query("SELECT * FROM chat_message_table")
    fun getAllChatMessages(): Flow<List<ChatMessage>>

//    @Query("SELECT * FROM chat_message_table")
//    fun getAllStory(): List<ChatMessage>



    @Query("SELECT * FROM chat_message_table WHERE senderId = :senderId AND receiverId = :receiverId")
    fun getChatMessagesBySenderId(senderId: Int,receiverId: Int): Flow<List<ChatMessage>>

    @Query("SELECT * FROM chat_message_table WHERE receiverId = :receiverId")
    fun getChatMessagesByReceiverId(receiverId: Int): List<ChatMessage>


}