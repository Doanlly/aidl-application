package com.example.finalserver.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.example.finalserver.User
import kotlinx.coroutines.flow.Flow

@Dao
interface UserDao{
    @Query("SELECT * FROM user_table")
    fun getAllUsers(): Flow<List<User>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUser(User: User)

    @Update
    suspend fun updateUser(User: User)
    @Query("SELECT * FROM user_table WHERE id = :userId")
    fun getUserById(userId: Int): Flow<User>

    @Query("SELECT * FROM User_table WHERE name LIKE :query")
    fun searchUsers(query: String): Flow<List<User>>

    @Query("SELECT * FROM User_table WHERE id = :classId")
    fun getUsersByClass(classId: String): List<User>

    @Query("SELECT * FROM User_table ORDER BY name ASC")
    fun getAllUsersSortedByName(): List<User>

}