package com.example.finalserver.views

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.finalserver.ChatMessage
import com.example.finalserver.R
import com.example.finalserver.adapter.ChatMessageAdapter
import com.example.finalserver.dataUser
import com.example.finalserver.databinding.FragmentChatListBinding
import com.example.finalserver.db.UserDao
import com.example.finalserver.db.UserDatabase
import com.example.finalserver.db.UserRepository
import com.example.finalserver.viewmodel.ChatListViewModel
import com.example.finalserver.viewmodel.ChatListViewModelFactory
import com.pmirkelam.ipcserver.RecentClient


class ChatListFragment : Fragment() {
    private lateinit var binding: FragmentChatListBinding
    private lateinit var chatMessageAdapter: ChatMessageAdapter
    private lateinit var userRepository: UserRepository
    private lateinit var userDatabase: UserDatabase
    private lateinit var userDao: UserDao
    private val viewModel: ChatListViewModel by viewModels {
        ChatListViewModelFactory(userRepository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userDatabase = UserDatabase.getDatabase(requireContext())
        userDao = userDatabase.UserDao()
        userRepository = UserRepository(userDao, userDatabase.ChatMessageDao())

        //load du lieu
        viewModel.loadData()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentChatListBinding.inflate(inflater, container, false)
        val view = binding.root
        chatMessageAdapter = ChatMessageAdapter(viewModel) { chatMessage ->
            val bundle = Bundle()
            bundle.putParcelable("chatMessage", chatMessage)
            findNavController().navigate(R.id.storyMessage, bundle)
            val client = RecentClient.client
            if (client != null) {
                Log.d("client chatList", "${client.chatMessage}")
            } else {
                Log.d("client", "Null")
            }
        }
        observeChatMessages()
        setupRecyclerView(view)

        return view
    }

    private fun setupRecyclerView(view: View) {
        val linearLayoutManager = LinearLayoutManager(requireContext())
        binding.recyclerview.layoutManager = linearLayoutManager
        binding.recyclerview.adapter = chatMessageAdapter
    }

    private fun observeChatMessages() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.chatMessages.collect { chatMessages ->
                chatMessageAdapter.submitList(checkIdStory(chatMessages))
            }
        }
    }

    private fun checkIdStory(chatMessages: List<ChatMessage>): List<ChatMessage> {
        val messagesByConversationId = chatMessages.associateBy { it.senderId to it.receiverId }
        return messagesByConversationId.values.toList()
    }
}

