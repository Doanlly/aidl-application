package com.example.finalser

import androidx.fragment.app.viewModels


import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.text.TextUtils
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.finalserver.ChatMessage
import com.example.finalserver.IMyAiDL
import com.example.finalserver.User
import com.example.finalserver.dataUser
import com.example.finalserver.db.UserDatabase
import com.example.finalserver.db.UserRepository
import com.example.finalserver.message
import com.example.finalserver.storyChat
import com.example.finalserver.timeCheck
import com.example.finalserver.viewmodel.ChatListViewModel
import com.example.finalserver.viewmodel.ChatListViewModelFactory
import com.pmirkelam.ipcserver.Client
import com.pmirkelam.ipcserver.RecentClient
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class Server : Service() {
    private var receivedUser: User? = null

    //private var userRepository: UserRepository =  UserRepository(UserDatabase.getDatabase(ti),UserDatabase().ChatMessageDao())
    private lateinit var userDatabaseInstance: UserDatabase
    private lateinit var userRepository: UserRepository
    private val userListLiveData = MutableLiveData<List<User>>()
    private val serverScope = CoroutineScope(Dispatchers.IO)
    private val ioDispatcher = Dispatchers.IO
    private val chatMessagesLiveData = MutableLiveData<List<ChatMessage>>()

    companion object {
        var connectionCount: Int = 0

        const val NOT_SENT = "Not sent!"
    }

    override fun onCreate() {
        super.onCreate()
        userDatabaseInstance = UserDatabase.getDatabase(this)
        // Khởi tạo UserRepository
        userRepository =
            UserRepository(userDatabaseInstance.UserDao(), userDatabaseInstance.ChatMessageDao())
        serverScope.launch {
            userRepository.getAllChatMessagesFlow().collect { chatMessages ->
                chatMessagesLiveData.postValue(chatMessages)
            }
        }
    }

    // AIDL IPC - Binder object to pass to the client
    private val aidlBinder = object : IMyAiDL.Stub() {
        override fun getMessageTime(): Long {
            return timeCheck
        }

        override fun getChatMessage(): ChatMessage? {
            return message
        }

        override fun getStory(): MutableList<ChatMessage> {
            return storyChat//chatMessages.toMutableList()
        }

        override fun getUser(): List<User> {

            return dataUser
        }

        override fun setDisplayedValue(packageName: String?, data: String?, data2: ChatMessage) {
            val clientData =
                if (data == null || TextUtils.isEmpty(data)) NOT_SENT
                else data
            RecentClient.client = Client(
                packageName,
                clientData,
                data2
            )
        }
    }

    override fun onBind(intent: Intent?): IBinder? {
        connectionCount++

        return aidlBinder

    }


    // A client has unbound from the service
    override fun onUnbind(intent: Intent?): Boolean {
        RecentClient.client = null

        return super.onUnbind(intent)
    }


}


//                RecentClient.client = Client(
//                    packageName ?: NOT_SENT,
//                    clientData,
//                    data2
//                )

//            receivedUser = User
//
//            if (receivedUser != null) {
//                val UserName = User?.name
//                val UserID = User?.id
//                val UserIMG = User?.imgPath
//
//                Log.d(ContentValues.TAG, "Received User: Name: $UserName, Age: $UserID, Class: $UserIMG")
//            } else {
//                // Không nhận được đối tượng User
//                Log.d(ContentValues.TAG, "No User received from Client")
//            }


//        override fun getStory(): MutableList<ChatMessage> {
//            val chatMessages = mutableListOf<ChatMessage>()
//
//            serverScope.launch {
//                userRepository.getAllChatMessagesFlow().collect { chatMessagesList ->
//                    Log.d("ChatMessage Service","$chatMessagesList")
//                    chatMessagesList.forEach { chatMessage ->
//                        chatMessages.add(chatMessage)
//                    }
//                }
//            }
//
//            return chatMessages
//        }