package com.example.finalserver.db

import androidx.lifecycle.LiveData
import com.example.finalserver.ChatMessage
import com.example.finalserver.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext

class UserRepository(private val UserDao: UserDao, private val chatMessageDao: ChatMessageDao) {
    val allUsers: Flow<List<User>> = UserDao.getAllUsers()

    //val allChatMessages: List<ChatMessage> = chatMessageDao.getAllStory()

    suspend fun insertUser(user: User) {
        UserDao.insertUser(user)
    }

    fun getUserById(userId: Int): Flow<User> {
        return UserDao.getUserById(userId)
    }
    fun getAllUsersFlow(): Flow<List<User>> {
        return UserDao.getAllUsers()
    }


    suspend fun insertChatMessage(chatMessage: ChatMessage) {
        chatMessageDao.insertChatMessage(chatMessage)
    }
    suspend fun insertChatMessages(chatMessages: List<ChatMessage>) {
        incrementChatMessageAutoIncrement()
        chatMessages.forEach { chatMessage ->
            chatMessageDao.insertChatMessage(chatMessage)
        }
    }


    fun getAllChatMessagesFlow(): Flow<List<ChatMessage>> {

        return chatMessageDao.getAllChatMessages()
    }
    fun getChatMessagesBySenderReceiver(senderId: Int,receiverId: Int): Flow<List<ChatMessage>> {
        return chatMessageDao.getChatMessagesBySenderId(senderId,receiverId)
    }


    suspend fun incrementChatMessageAutoIncrement(): Int {
        return chatMessageDao.getMaxId()
        //chatMessageDao.updateAutoIncrement(maxId + 1)
    }
}