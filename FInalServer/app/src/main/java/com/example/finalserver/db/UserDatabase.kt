package com.example.finalserver.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.finalserver.ChatMessage
import com.example.finalserver.User

@Database(entities = [User::class,ChatMessage::class], version = 1, exportSchema = false)
abstract class UserDatabase : RoomDatabase() {

    abstract fun UserDao(): UserDao
    abstract fun ChatMessageDao(): ChatMessageDao

    companion object {
        @Volatile
        private var INSTANCE: UserDatabase? = null
        fun getDatabase(context: Context): UserDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance =
                    Room.databaseBuilder(context.applicationContext,
                        UserDatabase::class.java,
                        "User_database"
                    ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}
