package com.example.finalserver.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import com.example.finalserver.ChatMessage
import com.example.finalserver.databinding.ItemChatMessageBinding
import com.example.finalserver.messageTime
import com.example.finalserver.viewmodel.ChatListViewModel
import com.example.finalserver.views.ChatListFragment
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class ChatMessageAdapter(
    private val viewModel: ChatListViewModel,
    private val onChatMessageClickListener: (ChatMessage) -> Unit
) : RecyclerView.Adapter<ChatMessageAdapter.ViewHolder>() {
    private var chatMessages: List<ChatMessage> = emptyList()
    private var userJob: Job? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemChatMessageBinding.inflate(inflater, parent, false)
        return ViewHolder(binding, parent.context as LifecycleOwner)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val chatMessage = chatMessages[position]
        holder.bind(chatMessage)
    }

    override fun getItemCount(): Int {
        return chatMessages.size
    }

    inner class ViewHolder(
        private val binding: ItemChatMessageBinding,
        private val lifecycleOwner: LifecycleOwner
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(chatMessage: ChatMessage) {
            binding.recentMesTxt.text = chatMessage.messageContent
            binding.btnTimeSent.text = chatMessage.messageTime.toString()
            userJob = lifecycleOwner.lifecycleScope.launch {
                viewModel.fetchUserById(chatMessage.senderId)
                    .collect { user ->
                        binding.nameTxtItem.text = user?.name ?: "Unknown User"
                    }
            }

            binding.cartViewItem.setOnClickListener {
                //Toast.makeText(this,"Click item",Toast.LENGTH_SHORT).show()
                Log.d("CLick chatMessage", "id: ${chatMessage.id}- ${chatMessage.messageContent}")
                //viewModel.onChatMessageClicked(chatMessage, ChatListFragment())
                onChatMessageClickListener.invoke(chatMessage)
            }
        }
    }

    fun submitList(chatMessages: List<ChatMessage>) {
        this.chatMessages = chatMessages
        notifyDataSetChanged()
    }
}


