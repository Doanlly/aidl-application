package com.example.finalserver

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import com.example.finalserver.databinding.ActivityMainBinding
import com.example.finalserver.views.ChatListFragment

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.my_nav_host_fragment) as NavHostFragment
        // Add the ChatListFragment to the activity's fragment manager
        supportFragmentManager.beginTransaction()
            .add(R.id.fragment_container, ChatListFragment())
            .commit()
        val navController = navHostFragment.navController
    }
}
