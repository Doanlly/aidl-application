package com.example.finalserver.viewmodel

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.example.finalserver.ChatMessage
import com.example.finalserver.R
import com.example.finalserver.User
import com.example.finalserver.dataUser
import com.example.finalserver.db.UserDatabase
import com.example.finalserver.db.UserRepository
import com.example.finalserver.storyChat
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

class ChatListViewModel(private val userRepository: UserRepository) : ViewModel() {
    private val _navigateToChatMessage = MutableStateFlow<ChatMessage?>(null)
    val navigateToChatMessage: StateFlow<ChatMessage?> = _navigateToChatMessage
    private val _user = MutableStateFlow<User?>(null)
    val user: StateFlow<User?> get() = _user
    val chatMessages: StateFlow<List<ChatMessage>> = userRepository.getAllChatMessagesFlow()
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(), emptyList())

    fun onChatMessageClicked(chatMessage: ChatMessage, fragment: Fragment) {
        _navigateToChatMessage.value = chatMessage
    }


    suspend fun insertChatMessages(chatMessages: List<ChatMessage>) {
        userRepository.insertChatMessages(chatMessages)
    }

    suspend fun insertChatMessage(chatMessage: ChatMessage) {
        userRepository.insertChatMessage(chatMessage)
    }

    suspend fun insertUser(user: User) {
        userRepository.insertUser(user)
    }

    fun fetchUserById(userId: Int): Flow<User?> {
        return userRepository.getUserById(userId)
            .catch {
                /* xu ly exceptions */
            }
    }

    fun getChatMessagesBySenderReceiver(senderId: Int, receiverId: Int): Flow<List<ChatMessage>> {
        return userRepository.getChatMessagesBySenderReceiver(senderId, receiverId)
    }

    fun loadData() {
        viewModelScope.launch(Dispatchers.IO) {
            val existingUsers = userRepository.allUsers.firstOrNull()//.firstOrNull()
            val existingChatMessages =
                userRepository.getAllChatMessagesFlow()
                    .firstOrNull()//allChatMessages.firstOrNull()//getAllChatMessages().firstOrNull()
            Log.d("existingUsers", "$existingUsers")
            Log.d("existingChatMessages", "$existingChatMessages")
            if (existingUsers != null && existingChatMessages != null) {
                if (existingUsers.isEmpty() && existingChatMessages.isEmpty()) {
                    userRepository.insertUser(dataUser[1])
                    userRepository.insertUser(dataUser[2])
                    userRepository.insertUser(dataUser[3])
                    userRepository.insertUser(dataUser[0])

                    userRepository.insertChatMessages(storyChat)
                } else {
                }
            }
        }
    }


}