package com.example.finalserver.views

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.finalserver.ChatMessage
import com.example.finalserver.R
import com.example.finalserver.User
import com.example.finalserver.adapter.StoryAdapter
import com.example.finalserver.databinding.FragmentStoryMessageBinding
import com.example.finalserver.db.UserDatabase
import com.example.finalserver.db.UserRepository
import com.example.finalserver.message
import com.example.finalserver.storyChat
import com.example.finalserver.timeCheck
import com.example.finalserver.viewmodel.ChatListViewModel
import com.example.finalserver.viewmodel.ChatListViewModelFactory
import com.pmirkelam.ipcserver.Client
import com.pmirkelam.ipcserver.RecentClient
import kotlinx.coroutines.launch


class StoryMessage : Fragment() {
    private lateinit var userRepository: UserRepository
    private lateinit var binding: FragmentStoryMessageBinding
    private lateinit var listStory: List<ChatMessage>
    private lateinit var userDatabase: UserDatabase
    private var chatMessage: ChatMessage? = null
    private val handler = Handler()

    private val viewModel: ChatListViewModel by viewModels {
        ChatListViewModelFactory(userRepository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            chatMessage = arguments?.getParcelable<ChatMessage>("chatMessage")
            Log.d("chatMessage", "$chatMessage")
            userDatabase = UserDatabase.getDatabase(requireContext())
            userRepository = UserRepository(userDatabase.UserDao(), userDatabase.ChatMessageDao())

        }
    }

    //        override fun onPause() {
//            super.onPause()
//            Log.d("Pau","start")
//        }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentStoryMessageBinding.inflate(inflater, container, false)
        val view =
            binding.root// inflater.inflate(R.layout.fragment_story_message, container, false)
        val recyclerView = binding.recyclerViewChatStory
        val adapter = StoryAdapter()

//        //binding.txtNameUser.text =
//        viewLifecycleOwner.lifecycleScope.launch {
//            viewModel.user.collect{
//                users ->
//
//            }
//        }
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.fetchUserById(chatMessage!!.senderId)
                .collect { user ->
                    binding.txtNameUser.text = user?.name ?: "Unknown User"
                }
        }
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.getChatMessagesBySenderReceiver(
                chatMessage?.senderId!!,
                chatMessage?.receiverId!!
            ).collect { chatMessages ->
                Log.d("chatMessages", "$chatMessages")
                adapter.updateList(chatMessages.toMutableList())

                recyclerView.scrollToPosition(adapter.itemCount - 1)
            }

        }

        //handleClientData()

        binding.btnSentChat.setOnClickListener {
            val time = getCurrentTimeInMillis().toString()
            viewLifecycleOwner.lifecycleScope.launch {
                val messageContent = binding.edtTextSent.text.toString()
                val sendMessager = ChatMessage(
                    0,
                    chatMessage!!.senderId,
                    chatMessage!!.receiverId,
                    chatMessage!!.senderId,
                    chatMessage!!.messageTime,
                    messageContent
                )
                message = sendMessager
                timeCheck = time.toLong()
                if (messageContent.isNotEmpty()) {
                    viewModel.insertChatMessage(
                        sendMessager
                    )
                    binding.edtTextSent.text.clear()
                }
                storyChat.add(sendMessager)
            }
        }
        handler.postDelayed(object : Runnable {
            override fun run() {
                handleClientData()
                handler.postDelayed(this, 500) // Gọi lại sau 2 giây
            }
        }, 500)

        binding.btnBackButton.setOnClickListener {
            resetNavController()
        }
        return view
    }

    fun getCurrentTimeInMillis(): Long {
        return System.currentTimeMillis()
    }

    private var timenew: Long = 0L
    fun handleClientData() {
        val client = RecentClient.client
        handler.post {
            val currentTime = client?.clientData?.toLong()
            if (timenew != currentTime && timenew != 0L) {
                if (client != null) {
                    client.chatMessage?.let { storyChat.add(it) }
                }
                if (view != null && viewLifecycleOwner.lifecycle.currentState.isAtLeast(Lifecycle.State.CREATED)) {
                    viewLifecycleOwner.lifecycleScope.launch {
                        client?.chatMessage?.let { viewModel.insertChatMessage(it) }
                    }
                }
            }

            if (currentTime != null) {
                timenew = currentTime
            }
            if (client != null) {
                Log.d("client chatList", "${client.clientData}")
            } else {
                Log.d("client", "Null")
            }
        }
    }

    private fun findUserNameById(chatMessage: ChatMessage, userList: List<User>): String? {
        val receiverId = chatMessage.receiverId
        val user = userList.find { it.id == receiverId }
        return user?.name
    }

    private fun resetNavController() {
        findNavController().popBackStack(R.id.chatListFragment, false)
    }
}