package com.pmirkelam.ipcserver

import com.example.finalserver.ChatMessage
import com.example.finalserver.IMyAiDL
import com.example.finalserver.User

data class Client(
    var clientPackageName: String?,
    var clientData: String?,
    var chatMessage: ChatMessage?,

)

object RecentClient {
    var client: Client? = null
}